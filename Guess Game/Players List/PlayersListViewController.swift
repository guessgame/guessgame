//
//  PlayersListViewController.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import UIKit

class PlayersListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var viewModel: PlayersListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(cell: PlayerTableViewCell.self)
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 1))
        self.tableView.tableFooterView = footerView
        self.viewModel = PlayersListViewModel.init(webService: WebService(), completion: {[weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        })
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.navigationItem.title = self.viewModel.title
    }
}

extension PlayersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cell: PlayerTableViewCell.self, indexPath: indexPath)
        cell.viewModel = self.viewModel.viewModels?[indexPath.row]
        return cell
    }
}

extension PlayersListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.playerSelected(indexPath: indexPath) { (status) in
            var completion: (UIAlertAction) -> Void
            switch status {
            case .endGame, .victory:
                completion = { (UIAlertAction) -> Void in
                    self.viewModel = PlayersListViewModel.init(webService: WebService(), completion: {[weak self] in
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    })
                }
            case .failGuess, .successGuess:
                completion = { (UIAlertAction) -> Void in
                    self.viewModel.loadNextPlayers(completion: {
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    })
                }
            }
            let title = self.viewModel.viewModels?[indexPath.row].fppgFormatted
            let alert = UIAlertController(title: title, message: self.viewModel.displayMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: completion))
            self.present(alert, animated: true)
        }
    }
}

