//
//  PlayerViewModel.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import Foundation

class PlayerViewModel {
    var player: Player
    
    init(player: Player) {
        self.player = player
    }
}

extension PlayerViewModel {
    var formattedName: String {
        get {
            let char = self.player.firstName?.first
            return "QB \(char ?? " ") \(self.player.lastName ?? "")"
        }
    }
    
    var formattedSalary: String {
        get {
            let double = Double(self.player.salary ?? 0)
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            currencyFormatter.locale = Locale.current
            currencyFormatter.minimumFractionDigits = 0
            return currencyFormatter.string(from: NSNumber(value: double)) ?? ""
        }
    }
    
    var formattedPosition: String {
        get {
            return "PHI @ \(self.player.position ?? "")"
        }
    }
    
    var fppgFormatted: String {
        get {
            return String(format: "%.02f", self.player.fppg ?? 0)
        }
    }
    
    var safeUrl: String {
        get {
            return self.player.images?._default?.url ?? ""
        }
    }
    
    var safePlayed: String {
        get {
            return String(self.player.played ?? 0)
        }
    }
}
