//
//  PlayerTableViewCell.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import UIKit
import SDWebImage
class PlayerTableViewCell: UITableViewCell {
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var playedLabel: UILabel!
    
    var viewModel: PlayerViewModel? {
        didSet {
            if let _ = self.viewModel {
                updateUI()
            }
        }
    }
    
    func updateUI() {
        self.playerImageView.sd_setImage(with: URL.init(string: self.viewModel?.safeUrl ?? ""), completed: nil)
        self.nameLabel.text = self.viewModel?.formattedName
        self.salaryLabel.text = self.viewModel?.formattedSalary
        self.positionLabel.text = self.viewModel?.formattedPosition
        self.playedLabel.text = self.viewModel?.safePlayed
    }
}
