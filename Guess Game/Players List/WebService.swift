//
//  WebService.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import Foundation
public typealias CompletionHandler<T: Codable> = (T?, Error?) -> Void
class WebService {
    func performURLSession<T: Codable> (codableType: T.Type, endpoint: String?, completionHandler: @escaping CompletionHandler<T>) {
        guard let endpoint = endpoint else { return }
        guard let url = URL(string: endpoint) else {
            print("Error: cannot create URL")
            completionHandler(nil, nil)
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {
                print(error!)
                completionHandler(nil, error)
                return
            }
            if let responseData = data {
                do {
                    let responseModel = try JSONDecoder().decode(codableType.self, from: responseData)
                    completionHandler(responseModel, nil)
                    return
                } catch {
                    completionHandler(nil, error)
                }
            }
        }
        task.resume()
    }
}
