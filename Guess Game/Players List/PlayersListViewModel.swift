//
//  PlayersListViewModel.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import Foundation
let endpoint = "https://gist.githubusercontent.com/liamjdouglas/bb40ee8721f1a9313c22c6ea0851a105/raw/6b6fc89d55ebe4d9b05c1469349af33651d7e7f1/Player.json"
let successGuessMessage = "Good guess"
let failGuess = "That is not correct, try again"
let endGameMessage = "There are no more available players, please restart the game"
let victoryMessage = "You are victorious"

enum GameStatus {
    case victory
    case successGuess
    case failGuess
    case endGame
    
}
class PlayersListViewModel {
    var title = "Guess Game"
    var webService: WebService
    var viewModels: [PlayerViewModel]?
    var seenPlayers = [Player]()
    var allPlayers: [Player]?
    var displayMessage = ""
    var correctGuesses = 0
    
    init(webService: WebService, completion: @escaping () -> Void) {
        self.webService = webService
        self.webService.performURLSession(codableType: PlayersResponse.self, endpoint: endpoint) { (response, error) in
            if let response = response, let players = response.players {
                self.allPlayers = players
                self.loadNextPlayers(completion: completion)
            } else {
                completion()
            }
        }
    }
    
    func loadNextPlayers(completion: () -> Void) {
        if let slice = self.allPlayers?.filter({ (player) -> Bool in
            return !self.seenPlayers.contains(where: { (seenPlayer) -> Bool in
                seenPlayer.id == player.id
            })
        }).shuffled().prefix(5) {
            self.viewModels = Array(slice).map { (player) -> PlayerViewModel in
                return PlayerViewModel.init(player: player)
            }
        }
        completion()
    }
}


extension PlayersListViewModel {
    var itemCount: Int {
        get {
            return self.viewModels?.count ?? 0
        }
    }
    
    var haveAvailablePlayers: Bool {
        get {
            return (self.allPlayers?.filter({ (player) -> Bool in
                return  !(self.seenPlayers.contains(where: { (seenPlayer) -> Bool in
                    seenPlayer.id == player.id
                }))
            }).count ?? 0) > 1
        }
    }
    
    func checkIfHighest(indexPath: IndexPath) -> Bool {
        guard let viewModels = self.viewModels else {
            return false
        }
        let selectedFppg = viewModels[indexPath.row].player.fppg ?? 0
        var isHighset = true
        for viewModel in viewModels {
            if (viewModel.player.fppg ?? 0) > selectedFppg {
               isHighset = false
            }
        }
        self.displayMessage = isHighset ? successGuessMessage : failGuess
        if let player = self.viewModels?[indexPath.row].player {
            self.seenPlayers.append(player)
        }
        self.correctGuesses = isHighset ? correctGuesses + 1 : correctGuesses
        return isHighset
    }
    
    var gameEndedSuccess: Bool {
        get {
            return self.correctGuesses >= 10
        }
    }
    
    func playerSelected(indexPath: IndexPath, completion: (_ status: GameStatus) -> Void) {
        let isHighest = self.checkIfHighest(indexPath: indexPath)
        if !isHighest {
            if self.haveAvailablePlayers {
                completion(.failGuess)
            } else {
                self.displayMessage = endGameMessage
                completion(.endGame)
            }
        } else {
            if self.gameEndedSuccess {
                self.displayMessage = victoryMessage
                completion(.victory)
            } else {
                completion(.successGuess)
            }
        }
    }
}
