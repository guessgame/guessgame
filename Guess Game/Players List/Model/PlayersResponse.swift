//
//  PlayersResponse.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import Foundation

struct PlayersResponse: Codable {
    let players: [Player]?  
}

struct Player: Codable {
    let id: String
    let firstName: String?
    let lastName: String?
    let fppg: Double?
    let images: Images?
    let played: Int?
    let position: String?
    let salary: Int?
    let news: News?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case fppg = "fppg"
        case images = "images"
        case played = "played"
        case position = "position"
        case salary = "salary"
        case news = "news"
    }
}
struct Images: Codable {
    let _default: Default?
    enum CodingKeys: String, CodingKey {
        case _default = "default"
    }
}
struct Default: Codable {
    let url: String?
}

struct News: Codable {
    let latest: String?
}

