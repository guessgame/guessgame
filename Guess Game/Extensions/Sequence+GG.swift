//
//  Sequence+GG.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import Foundation

extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        return Dictionary(grouping: self, by: key)
    }
    
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}

extension MutableCollection {
    mutating func shuffle() {
        for i in indices.dropLast() {
            let diff = distance(from: i, to: endIndex)
            let j = index(i, offsetBy: numericCast(arc4random_uniform(numericCast(diff))))
            swapAt(i, j)
        }
    }
}
