//
//  UITableView+GG.swift
//  Guess Game
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func register<T : UITableViewCell>(cell: T.Type) {
        let className = String(describing: T.self)
        register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: className)
    }
    
    func dequeue<T : UITableViewCell>(cell: T.Type, indexPath: IndexPath) -> T {
        let className = String(describing: T.self)
        return dequeueReusableCell(withIdentifier: className, for: indexPath) as! T
    }
}
