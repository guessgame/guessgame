 //
 //  ListViewModelTest.swift
 //  Guess GameTests
 //
 //  Created by Angelo gkiata on 16/09/2018.
 //  Copyright © 2018 Angelo gkiata. All rights reserved.
 //
 
 import XCTest
 @testable import Guess_Game
 class ListViewModelTest: XCTestCase {
    var mockViewModel: MockListViewModel!
    
    override func setUp() {
        super.setUp()
        self.mockViewModel = MockListViewModel(webService: MockWebService(), completion: {
            
        })
    }
    
    override func tearDown() {
        self.mockViewModel = nil
        super.tearDown()
    }
    
    func testFailGuess() {
        let indexPath = IndexPath.init(row: 1, section: 0)
        self.mockViewModel.playerSelected(indexPath: indexPath) { (status) in
            XCTAssertEqual(status , GameStatus.failGuess, "The Guess is correct")
            XCTAssertEqual("That is not correct, try again", self.mockViewModel.displayMessage, "The message is wrong")
        }
    }
    
    func testSucessGuess() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        self.mockViewModel.playerSelected(indexPath: indexPath) { (status) in
            XCTAssertEqual(status, GameStatus.successGuess, "The Guess is not correct")
            XCTAssertEqual("Good guess", self.mockViewModel.displayMessage, "The message is wrong")
        }
    }
    
    func testVictory() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        self.mockViewModel.correctGuesses = 9
        self.mockViewModel.playerSelected(indexPath: indexPath) { (status) in
            XCTAssertEqual(status, GameStatus.victory, "The not victorious")
            XCTAssertEqual("You are victorious", self.mockViewModel.displayMessage, "The message is wrong")
        }
    }
    
    func testEndGame() {
        let indexPath = IndexPath.init(row: 3, section: 0)
        let slice =  self.mockViewModel.allPlayers?.prefix(3)
        self.mockViewModel.seenPlayers.append(contentsOf: Array(slice!))
        self.mockViewModel.playerSelected(indexPath: indexPath) { (status) in
            XCTAssertEqual(status, GameStatus.endGame, "The is not finished")
            XCTAssertEqual("There are no more available players, please restart the game", self.mockViewModel.displayMessage, "The message is wrong")
        }
    }
 }
 
 
 class MockWebService: WebService {
    override func performURLSession<T>(codableType: T.Type, endpoint: String?, completionHandler: @escaping (T?, Error?) -> Void) where T : Decodable, T : Encodable {
        let path = Bundle(for: type(of: self)).path(forResource: "players", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        let mockResponse = try! JSONDecoder().decode(PlayersResponse.self, from: data)
        completionHandler(mockResponse as? T, nil)
    }
 }
 
 class MockListViewModel: PlayersListViewModel {
    override func loadNextPlayers(completion: () -> Void) {
        if let slice = self.allPlayers?.filter({ (player) -> Bool in
            return !self.seenPlayers.contains(where: { (seenPlayer) -> Bool in
                seenPlayer.id == player.id
            })
        }).prefix(5) {
            self.viewModels = Array(slice).map { (player) -> PlayerViewModel in
                return PlayerViewModel.init(player: player)
            }
        }
        completion()
    }
 }
