//
//  Guess_GameTests.swift
//  Guess GameTests
//
//  Created by Angelo gkiata on 16/09/2018.
//  Copyright © 2018 Angelo gkiata. All rights reserved.
//

import XCTest
@testable import Guess_Game

class Guess_GameTests: XCTestCase {
    var mockPlayerViewModel: PlayerViewModel!
    
    override func setUp() {
        super.setUp()
        let path = Bundle(for: type(of: self)).path(forResource: "player", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        let mockPlayer = try! JSONDecoder().decode(Player.self, from: data)
        self.mockPlayerViewModel = PlayerViewModel(player: mockPlayer)
    }
    
    override func tearDown() {
        self.mockPlayerViewModel = nil
        super.tearDown()
    }
    
    func testPlayerViewModel() {
        XCTAssertEqual("QB S Curry", mockPlayerViewModel.formattedName, "The Player's name is not formatted properly")
        
        XCTAssertEqual("$10,600", mockPlayerViewModel.formattedSalary, "The Player's salary is not formatted properly")
        
        XCTAssertEqual("47.94", mockPlayerViewModel.fppgFormatted, "The Player's salary is not formatted properly")
    }
}
